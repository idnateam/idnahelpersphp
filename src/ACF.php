<?php

namespace iDNA\Helpers;

class ACF
{

    private static $instance;

    function __construct()
    {
        if(self::funcsExist(['add_filter', 'add_action'])){
            if (class_exists('acf_field')) {
                add_action('acf/input/admin_head', array($this, 'acf_admin_head_block_preview'));
				add_filter('acf/get_taxonomies', array($this, 'acf_tax_nav_menu_add'));
            }
        }
    }

    public static function init() {
        if (!isset(self::$instance)) {
            self::$instance = new ACF();
        }
        return self::$instance;
    }

	public function acf_tax_nav_menu_add($taxonomies)
	{
		$taxonomies[] = 'nav_menu';
		return $taxonomies;
	}

    public function acf_admin_head_block_preview()
    {
		$images_path = 'block-images';
        
        //Works with both sage 9 and sage 10
        $module_images_path = str_replace('/resources', '', get_stylesheet_directory_uri()) . '/resources/' . $images_path . '/';
		$abs_path = get_theme_file_path() . '/resources/' . $images_path . '/';

		if (!file_exists($abs_path)) {
			mkdir($abs_path, 0755);
		}

        if (file_exists($abs_path) AND is_dir($abs_path)) {
            ?>
            <style type="text/css">
                .acf-tooltip.acf-fc-popup.bottom {
                    max-width: 100%;
                }
                .acf-tooltip.acf-fc-popup.top {
                    max-width: 100%;
                }
                .imagePreview {
                    position: absolute;
                    right: 100%;
                    top: 0px;
                    z-index: 999999;
                    border: 1px solid #f2f2f2;
                    box-shadow: 0px 0px 9px #b6b6b6;
                    background-color: #2F353E;
                    border-radius: 5px;
                    padding: 5px;
                }

                .imagePreview img {
                    width: 500px;
                    height: auto;
                    display: block;
                }

                .acf-tooltip li:hover {
                    background-color: #0074a9;
                }
            </style>
            <script>
                jQuery(document).ready(function($) {
                    $(document).on('click', "a[data-name=add-layout]", function() {
                        waitForEl(".acf-tooltip li", function() {
                            $(".acf-tooltip li a").hover(function() {
                                imageTP = $(this).attr("data-layout");
                                imageFullPath = "<?php echo $module_images_path; ?>" + imageTP + '.png';
								checkIfImageExists(imageFullPath, (exists) => {
									if (exists) {
										$(".acf-tooltip").append('<div class="imagePreview"><img src="' + imageFullPath + '" /></div>');
									}
								});
                            }, function() {
                                $(".imagePreview").remove();
                            });
                        });
                    })
                    var waitForEl = function(selector, callback) {
                        if (jQuery(selector).length) {
                            callback();
                        } else {
                            setTimeout(function() {
                                waitForEl(selector, callback);
                            }, 100);
                        }
                    };
					function checkIfImageExists(url, callback) {
						const img = new Image();

						img.src = url;

						if (img.complete) {
							callback(true);
						} else {
							img.onload = () => {
								callback(true);
							};
							img.onerror = () => {
								callback(false);
							};
						}
					}
                })
            </script>
        <?php

        }
    }

	/**
     * @param $vecFunc
     * @return bool
     */
    private static function funcsExist($vecFunc){
        foreach ($vecFunc as $func){
            if(!function_exists($func)){
                return false;
                break;
            }
        }
        return true;
    }
}