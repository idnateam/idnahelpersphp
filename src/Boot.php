<?php

namespace iDNA\Helpers;

class Boot
{

    private static $instance;

	function __construct() {
		require __DIR__.'/../vendor/autoload.php';
	}

    public static function init() {
        if (!isset(self::$instance)) {
            self::$instance = new Boot();
        }
        return self::$instance;
    }
}
