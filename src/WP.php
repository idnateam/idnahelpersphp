<?php

namespace iDNA\Helpers;

class WP
{

    private $bootstrap_version;

	function __construct($bootstrap_version = 4) {

        $this->bootstrap_version = $bootstrap_version;

		if(self::funcsExist(['add_filter','add_action','remove_action'])){

            if ($this->bootstrap_version == 5) {
                add_filter('embed_oembed_html', function ($html, $url, $attr, $post_id) {
                    $html = preg_replace( '/(width|height|frameborder)="\d*"\s/', "", $html);
                    $html = str_replace('<iframe', '<iframe class="embed-responsive-item"', $html);
    
                    preg_match('/src="(.+?)"/', $html, $matches);
                    $src = $matches[1];
    
                    // add extra params to iframe src
                    $params = array(
                            'rel'    => 0,
                    );
    
                    $new_src = add_query_arg($params, $src);
    
                    $html = str_replace($src, $new_src, $html);
    
                    $attributes = 'frameborder="0"';
    
                    $html = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $html);
    
                    return '<div class="ratio ratio-16x9">'.$html.'</iframe></div>';
                }, 99, 4);
            } else {
                add_filter('embed_oembed_html', function ($html, $url, $attr, $post_id) {
                    $html = preg_replace( '/(width|height|frameborder)="\d*"\s/', "", $html);
                    $html = str_replace('<iframe', '<iframe class="embed-responsive-item"', $html);
    
                    preg_match('/src="(.+?)"/', $html, $matches);
                    $src = $matches[1];
    
                    // add extra params to iframe src
                    $params = array(
                            'rel'    => 0,
                    );
    
                    $new_src = add_query_arg($params, $src);
    
                    $html = str_replace($src, $new_src, $html);
    
                    $attributes = 'frameborder="0"';
    
                    $html = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $html);
    
                    return '<div class="embed-responsive embed-responsive-16by9 le-embed-custom">'.$html.'</iframe></div>';
                }, 99, 4);
            }

			add_action('wp_head', function (){
				echo "<script> var idnah_ajaxurl = '".admin_url('admin-ajax.php')."'; </script>";
			});

			add_action('wp_ajax_idna_load_more', array( $this, 'idnaLoadMore' ));
			add_action('wp_ajax_nopriv_idna_load_more', array( $this, 'idnaLoadMore' ));

		}
	}

    public function overrideLoadMoreAction($func){
        if(has_action('wp_ajax_idna_load_more')){
            remove_action('wp_ajax_idna_load_more', array( $this, 'idnaLoadMore' ));
        }
        if(has_action('wp_ajax_nopriv_idna_load_more')){
            remove_action('wp_ajax_nopriv_idna_load_more', array( $this, 'idnaLoadMore' ));
        }

        add_action('wp_ajax_idna_load_more', $func);
        add_action('wp_ajax_nopriv_idna_load_more', $func);

    }

    /**
     * Init iDNA Helpers
     */
    public static function idnaLoadMore(){

        $ret = array(
            'status' => false,
            'elements' => array(),
        );

        try{
            if(isset($_POST['post_type']) AND isset($_POST['template']) AND isset($_POST['posts_per_page']) AND isset($_POST['page']) ){
                $post_type = $_POST['post_type'];
                $template = base64_decode($_POST['template']);
                $posts_per_page = $_POST['posts_per_page'];
                $page = $_POST['page'];

                $tax_query = array();
                if(isset($_POST['terms'])){
                    $tax_query['relation'] = 'AND';
                    foreach ($_POST['terms'] as $term){
                        $tax_query[] = array(
                            'taxonomy' => $term['type'],
                            'terms' => $term['ids'],
                            'field' => 'term_id',
                            'include_children' => false,
                            'operator' => 'IN'
                        );
                    }
                }

                $args = array(
                    'post_type' => $post_type,
                    'tax_query' => $tax_query,
                    'posts_per_page' => $posts_per_page,
                    'paged' => $page,
                    'post_status' => 'publish',
                );

                $query = new \WP_Query($args);
                if ( $query->have_posts() ) {
                    $ret['status'] = true;
                    while ( $query->have_posts() ) {
                        $query->the_post();
                        $ret['elements'][] = self::loadBladePartContent($template);
                    }
                }
                wp_reset_postdata();

            }
        }catch (\Exception $e){
            // errore
            var_dump($e->getMessage());
        }

        wp_send_json($ret);
    }


    /**
     * @param $logo
     * @param $width
     * @param $height
     * @param $hidePostTypeLink
     * @param $extraStyles
     */
    private static function stylizeAdmin($logo, $width, $height, $hidePostTypeLink, $extraStyles) {
        ?>
        <style type="text/css">
            <?php
            if($logo AND $width AND $height){
                ?>
            .login h1 a {
                background-image: url(<?php echo $logo; ?>) !important;
                background-size: <?php echo $width; ?>px <?php echo $height; ?>px !important;
                width:<?php echo $width; ?>px !important;
                height:<?php echo $height; ?>px !important;
                margin: 0 auto 20px auto !important;
            }
            <?php
        }
        if($hidePostTypeLink){
            foreach ($hidePostTypeLink as $type){
                ?>
            body.wp-admin.post-type-<?php echo $type; ?> #preview-action, body.wp-admin.post-type-<?php echo $type; ?> #edit-slug-box, body.wp-admin.post-type-<?php echo $type; ?> #message.updated.notice-success p a, body.wp-admin.post-type-<?php echo $type; ?> #posts-filter .row-actions .view{ display: none !important; }
            <?php
        }
    }
    if($extraStyles){
        echo $extraStyles;
    }
    ?>
        </style>
        <?php
    }

    /**
     * @param bool|string $logo
     * @param bool|integer $width
     * @param bool|integer $height
     * @param array $hidePostTypeLink
     * @param bool|string $extraStyles
     */
    public static function stylizeAdminAddAction($logo = false, $width = false, $height = false, $hidePostTypeLink = [], $extraStyles = false)
    {
        if(self::funcsExist(['add_action'])){
            add_action( 'login_enqueue_scripts', function () use ($logo, $width, $height, $hidePostTypeLink, $extraStyles){
                self::stylizeAdmin($logo, $width, $height, $hidePostTypeLink, $extraStyles);
            });
            add_action('admin_print_styles', function () use ($logo, $width, $height, $hidePostTypeLink, $extraStyles){
                self::stylizeAdmin($logo, $width, $height, $hidePostTypeLink, $extraStyles);
            });
        }
    }


    /**
     * @param $template_name
     * @param array $data
     * @return false|string
     */
    public static function loadTemplatePartContent($template_name, $data = array())
    {
        try{
            ob_start();
            if(self::funcsExist(['set_query_var','get_template_part'])){
                foreach ($data as $key => $value) {
                    set_query_var($key, $value);
                }
                get_template_part($template_name);
            }
            $var = ob_get_contents();
            ob_end_clean();
            return $var;
        }catch (\Exception $e){
            return '';
        }
    }

    /**
     * @param $template_name
     * @param array $data
     * @return false|string
     */
    public static function loadBladePartContent($template_name, $data = array())
    {
        try{
            ob_start();
            $vecData = [];
            foreach ($data as $key => $value) {
                $vecData[] = ${$key} = $value;
            }
            if(self::funcsExist(['\App\template_path','locate_template'])){
                include \App\template_path(locate_template($template_name), $vecData);
            }
            $var = ob_get_contents();
            ob_end_clean();
            return $var;
        }catch (\Exception $e){
            //return '';
            return 'ERROR: ' . $e->getMessage();
        }
    }


    /**
     * @param $id
     * @param bool $lang
     * @param bool $esc_url
     * @return bool|string
     */
    public static function getPostEditLink($id, $lang = false, $esc_url = true)
    {

        // https://codex.wordpress.org/Template_Tags/get_edit_post_link
        // get_edit_post_link( $id, $context )

        $url = false;
        if(self::funcsExist(['get_site_url','esc_url'])){
            $id = intval($id);
            if ($lang) {
                $lang = "&lang=" . $lang;
            }
            $url = get_site_url() . '/wp-admin/post.php?post=' . $id . '&action=edit' . $lang;
            if ($esc_url) {
                return esc_url($url);
            }
        }
        return $url;
    }


    /**
     * @param $posts
     * @param $orderBy
     * @param string $order
     * @param bool $unique
     * @return array|bool|false
     */
    public static function orderArrayPostBy($posts, $orderBy, $order = 'ASC', $unique = true)
    {
        if (!is_array($posts)) {
            return false;
        }
        if(self::funcsExist(['\iDNA\Helpers\Sort_Posts','wp_list_pluck'])){
            usort($posts, array(new \iDNA\Helpers\Sort_Posts($orderBy, $order), 'sort'));
            // use post ids as the array keys
            if ($unique && count($posts)) {
                $posts = array_combine(wp_list_pluck($posts, 'ID'), $posts);
            }
        }

        return $posts;
    }


    /**
     * @param string $forceTitle
     * @param int $id
     * @return string
     */
    public static function getAltTag($forceTitle = false, $id = 0)
    {
        $return = '';
        if(self::funcsExist(['get_post_type','wp_get_attachment_metadata','get_the_title'])){
            if ($forceTitle) {
                return $forceTitle;
            } elseif ($id > 0) {

                $curr_type = get_post_type($id);

                if ($curr_type == 'attachment') {
                    $meta = wp_get_attachment_metadata($id);

                    if (isset($meta['image_meta']['title'])) {
                        $return = addslashes($meta['image_meta']['title']);
                    }

                } elseif (get_the_title($id)) {
                    $return = addslashes(get_the_title($id));
                }
            }
        }
        return $return;
    }

    /**
     * Utility di stampa (o ritorno html) di un attachment ID specifico
     *
     * @param int $attachment_id
     * @param string $size
     * @param array $attr
     * @param bool $initial_alt
     * @param bool $print
     * @return string
     */
    public static function printImage($attachment_id = 0, $size = 'thumbnail', $attr = [], $initial_alt = false, $print = true){
        $html = '';
        if(self::funcsExist(['wp_get_attachment_image_src','get_the_title','get_the_ID','get_post_meta'])){
            $img = wp_get_attachment_image_src($attachment_id, $size);
            if($img){
                $vec_attr = [];
                foreach ($attr as $key => $value){
                    $vec_attr[] = $key.'="'.$value.'"';
                }
                if(!$initial_alt){
                    $initial_alt = get_the_title(get_the_ID());
                }
                if(get_the_title($attachment_id)){
                    $initial_alt = get_the_title($attachment_id);
                }
                if(get_post_meta($attachment_id, '_wp_attachment_image_alt', TRUE)){
                    $initial_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', TRUE);
                }
                $vec_attr[] = 'alt="'.$initial_alt.'"';
                $html = '<img src="'.$img[0].'" '.implode(' ', $vec_attr).' />';
            }
        }
        if($print){
            echo $html;
        }else{
            return $html;
        }
    }

    /**
     * @param $file
     * @param $content
     * @param bool $encode
     * @param int $append pass zero to override
     * @return bool
     */
    public static function saveLog($file, $content, $encode = false, $append = FILE_APPEND){
        if(self::funcsExist(['get_post_type','file_put_contents'])){
            try{
                $upload_array = wp_upload_dir();
                $upload_dir = $upload_array['basedir'];
                if($encode){
                    $content = json_encode($content);
                }
                if($append===FILE_APPEND){
                    $content .= PHP_EOL;
                }
                file_put_contents($upload_dir . '/' . $file, $content, $append);
                return true;
            }catch (\Exception $e){
                return false;
            }
        }
        return false;
    }

    /**
     * @param $vecFunc
     * @return bool
     */
    private static function funcsExist($vecFunc){
        foreach ($vecFunc as $func){
            if(!function_exists($func)){
                return false;
                break;
            }
        }
        return true;
    }


    /**
     * Modification of "Build a tree from a flat array in PHP"
     *
     * Authors: @DSkinner, @ImmortalFirefly and @SteveEdson
     *
     * @link https://stackoverflow.com/a/28429487/2078474

     * @param array $elements
     * @param int $parentId
     * @return array
     */
    private static function buildTree( array &$elements, $parentId = 0 )
    {
        $branch = array();
        foreach ( $elements as &$element )
        {
            if ( $element->menu_item_parent == $parentId )
            {
                $children = self::buildTree( $elements, $element->ID );
                if ( $children ){
                    $element->item_children = $children;
                }

                $branch[$element->ID] = $element;
                unset( $element );
            }
        }
        return $branch;
    }

    /**
     * Transform a navigational menu to it's tree structure

     * @param $menu_id
     * @return array|null
     */
    public static function wpMenuToTree( $menu_id )
    {
        $items = wp_get_nav_menu_items( $menu_id );
        if($items){
            return self::buildTree( $items, 0);
        }else{
            return  null;
        }
    }

    /**
     * Rimuove le voci di menù nell'admin, è possibile passare le voci da rimuovere per tutti o solo per i NON ADMIN
     * @param array $removeForAll
     * @param array $removeForNONAdmin
     */
    public static function removeAdminMenuPage($removeForAll = [], $removeForNONAdmin = [])
    {
        if(self::funcsExist(['add_action'])){

            add_action('admin_menu', function() use ($removeForAll, $removeForNONAdmin){

                if($removeForAll){
                    foreach ($removeForAll as $page1){
                        remove_menu_page($page1);
                    }
                }
                if($removeForNONAdmin){
                    $user = wp_get_current_user();
                    if (!in_array("administrator", $user->roles)) {
                        foreach ($removeForNONAdmin as $page2){
                            remove_menu_page($page2);
                        }
                    }
                }
            });
        }
    }

    /**
     * Rimuove le voci in admin bar, è possibile passare le voci da rimuovere per tutti o solo per i NON ADMIN
     *
     * @param array $removeForAll
     * @param array $removeForNONAdmin
     */
    public static function removeAdminMenuBar($removeForAll = [], $removeForNONAdmin = [])
    {
        if(self::funcsExist(['add_action'])){
            add_action('wp_before_admin_bar_render', function() use ($removeForAll, $removeForNONAdmin){
                global $wp_admin_bar;
                if($removeForAll){
                    foreach ($removeForAll as $page1){
                        $wp_admin_bar->remove_menu($page1);
                    }
                }
                if($removeForNONAdmin){
                    $user = wp_get_current_user();
                    if (!in_array("administrator", $user->roles)) {
                        foreach ($removeForNONAdmin as $page2){
                            $wp_admin_bar->remove_menu($page2);
                        }
                    }
                }
            });
        }
    }

    /**
     * Disabilita le single per i post type specificati
     *
     * @param array $postTypes
     */
    public static function disableSingle($postTypes = [])
    {
        if(self::funcsExist(['add_action'])){
            add_action('template_redirect', function() use ($postTypes){
                $queried_post_type = get_query_var('post_type');
                if (is_single() && in_array($queried_post_type, $postTypes)) {
                    wp_redirect(home_url(), 301);
                    exit;
                }
            });

            $vecSelectors = [];
            foreach ($postTypes as $type){
                $vecSelectors[] = 'body.wp-admin.post-type-'.$type.' #preview-action, body.wp-admin.post-type-'.$type.' #edit-slug-box, body.wp-admin.post-type-'.$type.' #message.updated.notice-success p a, body.wp-admin.post-type-'.$type.' #posts-filter .row-actions .view';
            }
            add_action('login_enqueue_scripts', function () use ($vecSelectors){
                echo '<style type="text/css">'.implode(',',$vecSelectors).'{display: none !important;}</style>';
            });
            add_action('admin_print_styles', function () use ($vecSelectors){
                echo '<style type="text/css">'.implode(',',$vecSelectors).'{display: none !important;}</style>';
            });
        }
    }

    /**
     * Rinuove la thumbnail in admin per i tipi/template specificati in base agli array
     *
     * @param array $postTypes
     * @param array $templates (views/template-name.blade.php)
     * @param array $ids list of id page
	 * @param boolean $hideInFrontPage true/false for hide editor in home page
     */
	public static function removeThumbnail($postTypes = [], $templates = [], $ids = [], $hideInFrontPage = false)
	{
        global $pagenow;
        if(self::funcsExist(['add_action'])){
            add_action('admin_init', function() use ($postTypes, $templates, $ids, $hideInFrontPage, $pagenow){
                if($pagenow == 'post-new.php'){
                    $type = isset($_GET['post_type']) && !empty($_GET['post_type']) ? $_GET['post_type'] : 'post';
                    if(in_array($type, $postTypes)) {
                        remove_post_type_support($type, 'thumbnail');
                    }
                }else if (isset($_GET['post']) OR isset($_GET['post']) OR isset($_POST['post_ID'])) {
                    $post_id = isset($_GET['post']) ? $_GET['post'] : $_POST['post_ID'];
                    if (!isset($post_id)) return;
                    $template_file = get_post_meta($post_id, '_wp_page_template', true);
                    $post = get_post($post_id);

                    if(!empty($post)) {
                        if(in_array($post->post_type, $postTypes) OR in_array($template_file, $templates) OR in_array($post_id, $ids)){
                            remove_post_type_support($post->post_type, 'thumbnail');
                        }
                    }

                    if((int) get_option('page_on_front')==$post_id && $hideInFrontPage){
                        remove_post_type_support('page', 'thumbnail');
                    }
                }
            });
        }
	}

    /**
     * Rinuove l'editor in admin per i tipi/template specificati in base agli array
     *
     * @param array $postTypes
     * @param array $templates (views/template-name.blade.php)
     * @param array $ids list of id page
	 * @param boolean $hideInFrontPage true/false for hide editor in home page
     */
	public static function removeEditor($postTypes = [], $templates = [], $ids = [], $hideInFrontPage = false)
	{
        global $pagenow;
        if(self::funcsExist(['add_action'])){
            add_action('admin_init', function() use ($postTypes, $templates, $ids, $hideInFrontPage, $pagenow){
                if($pagenow == 'post-new.php'){
                    $type = isset($_GET['post_type']) && !empty($_GET['post_type']) ? $_GET['post_type'] : 'post';
                    if(in_array($type, $postTypes)) {
                        remove_post_type_support($type, 'editor');
                    }
                }else if (isset($_GET['post']) OR isset($_GET['post']) OR isset($_POST['post_ID'])) {
                    $post_id = isset($_GET['post']) ? $_GET['post'] : $_POST['post_ID'];
                    if (!isset($post_id)) return;
                    $template_file = get_post_meta($post_id, '_wp_page_template', true);
                    $post = get_post($post_id);

                    if(!empty($post)) {
                        if(in_array($post->post_type, $postTypes) OR in_array($template_file, $templates) OR in_array($post_id, $ids)){
                            remove_post_type_support($post->post_type, 'editor');
                        }
                    }

                    if((int) get_option('page_on_front')==$post_id && $hideInFrontPage){
                        remove_post_type_support('page', 'editor');
                    }
                }
            });
        }
	}

    /**
     * Rimuove gli attributi superflui sui tag script per una corretta validazione w3c
     */
    public static function disableJsCssAttributes(){
        if(self::funcsExist(['add_filter','remove_action'])){

            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('wp_print_styles', 'print_emoji_styles');
            remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
            remove_action( 'admin_print_styles', 'print_emoji_styles' );
            add_filter('style_loader_tag', function ($tag, $handle){
                return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
            }, 10, 2);
            add_filter('script_loader_tag', function ($tag, $handle){
                return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
            }, 10, 2);


            add_action('widgets_init', function (){
                global $wp_widget_factory;
                remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
            });

        }
    }


    /**
     * Utility per il ridimensionamento dei campi Wysiwyg ad un altezza personalizzata
     *
     * @param int $height
     */
    public static function resizeWysiwygHeight($height = 100){
        if(self::funcsExist(['add_action'])){

            add_action('acf/input/admin_footer', function () use ($height) {
                ?>
                <style>
                    .acf-editor-wrap iframe {
                        min-height: 0;
                    }
                </style>
                <script>
                    (function($) {
                        // (filter called before the tinyMCE instance is created)
                        acf.add_filter('wysiwyg_tinymce_settings', function(mceInit, id, $field) {
                            // enable autoresizing of the WYSIWYG editor
                            mceInit.wp_autoresize_on = false;
                            return mceInit;
                        });
                        // (action called when a WYSIWYG tinymce element has been initialized)
                        acf.add_action('wysiwyg_tinymce_init', function(ed, id, mceInit, $field) {
                            // reduce tinymce's min-height settings
                            ed.settings.autoresize_min_height = <?php echo $height; ?>;
                            // reduce iframe's 'height' style to match tinymce settings
                            $('.acf-editor-wrap iframe').css('height', '<?php echo $height; ?>px');
                        });
                    })(jQuery)
                </script>
                <?php
            });
        }
    }


	/**
	 * Returns all child nav_menu_items under a specific parent
	 *
	 * @param int the parent nav_menu_item ID
	 * @param array nav_menu_items
	 * @param bool gives all children or direct children only
	 *
	 * @return array returns filtered array of nav_menu_items
	 */
	public static function get_nav_menu_item_children( $parent_id, $nav_menu_items, $depth = true ) {
		$nav_menu_item_list = array();
		foreach ( (array) $nav_menu_items as $nav_menu_item ) {
			if ( $nav_menu_item->menu_item_parent == $parent_id ) {
				$nav_menu_item_list[] = $nav_menu_item;
				if ( $depth ) {
					if ( $children = get_nav_menu_item_children( $nav_menu_item->ID, $nav_menu_items ) ) {
						$nav_menu_item_list = array_merge( $nav_menu_item_list, $children );
					}
				}
			}
		}

		return $nav_menu_item_list;
	}

	public static function getIdMenuFromLocation( $location ) {
		$theme_locations = get_nav_menu_locations();

		$menu_obj = get_term( $theme_locations[ $location ], 'nav_menu' );

		$menu_id = $menu_obj->term_id;

		return $menu_id;
	}

	public static function getStaticPage($id = '' ) {
		if (empty($id)) { return ''; }
		$static_page_id = get_option($id);
		if (get_post_status($static_page_id) && $static_page_id != '0') {
			return $static_page_id;
		}
		return '';
	}

	public function addSettingsStaticPage( array $args = array() ) : void {
		if (!is_array($args) OR empty($args)) { return; }

		foreach ($args as $arg) {
			if (!isset($arg['label']) OR empty($arg['label']) OR !isset($arg['id']) OR empty($arg['id'])) {
				continue;
			}
			$label = $arg['label'];
			$id = $arg['id'];
			add_action('admin_init', function() use ($label, $id) {
				add_settings_field($id, $label, array($this, 'settings_field_static'), 'reading', 'default', array(
					'id'        => $id,
					'label_for' => 'field-' . $id,
					'class'     => 'row-' . $id
				));
			});
			add_filter('whitelist_options', function ($options) use ($id) {
				$options['reading'][] = $id;
				return $options;
			});
			add_filter('display_post_states', function($states) use ($label, $id) {
				global $post;
                
                if(!empty($post)) {
                    $static_page = get_option($id);
                    if ('page' == get_post_type($post->ID) && $post->ID == $static_page && $static_page != '0') {
                        $states[] = __($label, 'wp-static-page');
                    }
                }
                
				return $states;
			});
		}
	}

	public function settings_field_static($args)
	{
		wp_dropdown_pages(array(
			'name'              => $args['id'],
			'show_option_none'  => '&mdash; Seleziona &mdash;',
			'option_none_value' => '0',
			'selected'          => get_option($args['id']),
		));
	}
    
        //YOAST SEO

    /**
     * Remove Schema pieces (es. Remove Organization type)
     *
     * @param array             $data    Schema.org graph.
     * @param Meta_Tags_Context $context Context object.
     *
     * @return array The altered Schema.org graph.
     */
    public static function remove_organization_from_schema($pieceType) {

        if(self::funcsExist(['add_filter'])) {

            if(!empty($pieceType)) {
                add_filter( 'wpseo_schema_graph', function($data, $context) use ($pieceType) {    
                    for($i = 0; $i < count($data); $i++) {
                        if($data[$i]['@type'] == $pieceType) {
                            unset($data[$i]);
                        }
                    }
                    //dump($data);
            
                    return $data;
                }, 10, 2 );
            }
        }

    }

    /**
     * Customize images to Article Schema data.
     *
     * @param array             $data    Schema.org graph.
     * @param Meta_Tags_Context $context Context object.
     *
     * @return array The altered Schema.org graph.
     * 
     */
    public static function yoast_customize_article_img($post_type = 'post', $acfImageFieldLabel = '') {

        if(self::funcsExist(['add_filter'])) {

            add_filter( 'wpseo_schema_article', function($data) use($post_type, $acfImageFieldLabel) {

                if(is_singular($post_type) && !empty($acfImageFieldLabel)) {
    
                    //se l'immagine non è già settata uso quella custom tamite il campo acf
                    if(!isset($data['image']) && !isset($data['thumbnailUrl'])) {

                        $attachmentId = get_field($acfImageFieldLabel, get_the_ID());
                
                        if(!empty($attachmentId)) {
            
                            $canonical = YoastSEO()->meta->for_current_page()->canonical;
            
                            if(!empty($canonical)) {
                                $schema_id = $canonical . '#/schema/image/' . $attachmentId;                
                                $data['image'] = YoastSEO()->helpers->schema->image->generate_from_attachment_id($schema_id, $attachmentId);
                            }
            
                        }
            
                    }
                }

                //dump($data);

                return $data;

            });   
        }

    }
    
        /**
    * Add alt attribute to every image element
    */
    public function add_custom_image_data_attributes() {

        if(self::funcsExist(['add_filter'])) {

            add_filter( 'wp_get_attachment_image_attributes', function($attr, $attachment, $size) {
                // Ensure that the <img> doesn't have the alt attribute already

                $altAttribute = $attr['alt'];

                if (empty($altAttribute)) {
                    $altAttribute = $attachment->post_title;
                }

                //dump($altAttribute);

                $attr['alt'] = str_replace(['-', '_'], ' ', $altAttribute);

                return $attr;
            }, 10, 3 );

        }

    }
    
    /**
     * Add alt attribute to every image element in acf editor e wp editor
     */
    public function wpdocs_replaceALT() {

        if(self::funcsExist(['add_filter'])) {

            add_filter( 'the_content', function($content) {

                if ( !$content ) {
                    return $content;
                }
            
                libxml_use_internal_errors( true );
            
                $post = new \DOMDocument;
                $post->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
                $images = $post->getElementsByTagName( 'img' );
            
                foreach ( $images as $image ) {
                    $src = $image->getAttribute( 'src' );
                    $image_id = attachment_url_to_postid( $src );
                    $media = get_post( $image_id );
            
                    $altAttribute = $image->getAttribute( 'alt' );
            
                    if(empty($altAttribute)) {
                        $altAttribute = $media->post_title;
                    }
            
                    $image->setAttribute( 'alt', str_replace(['-', '_'], ' ', $altAttribute) );
                }
                $content = $post->saveHTML();
                return $content;

            }, 99 );

            add_filter( 'acf_the_content', function($content) {

                if ( !$content ) {
                    return $content;
                }
            
                libxml_use_internal_errors( true );
            
                $post = new \DOMDocument;
                $post->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
                $images = $post->getElementsByTagName( 'img' );
            
                foreach ( $images as $image ) {
                    $src = $image->getAttribute( 'src' );
                    $image_id = attachment_url_to_postid( $src );
                    $media = get_post( $image_id );
            
                    $altAttribute = $image->getAttribute( 'alt' );
            
                    if(empty($altAttribute)) {
                        $altAttribute = $media->post_title;
                    }
            
                    $image->setAttribute( 'alt', str_replace(['-', '_'], ' ', $altAttribute) );
                }
                $content = $post->saveHTML();
                return $content;

            }, 99 );

        }

    }

}