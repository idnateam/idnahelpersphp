<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'c65426aef5249e8ecc2f77b78ca96ed08ed03b4a',
        'name' => 'idna/helpers',
        'dev' => true,
    ),
    'versions' => array(
        'idna/helpers' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'c65426aef5249e8ecc2f77b78ca96ed08ed03b4a',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(
                0 => '1.23.x-dev',
            ),
            'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(
                0 => '1.23.x-dev',
            ),
            'reference' => '57b712b08eddb97c762a8caa32c84e037892d2e9',
            'dev_requirement' => true,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => '5.4.x-dev',
            'version' => '5.4.9999999.9999999-dev',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'reference' => '2366ac8d8abe0c077844613c1a4f0c0a9f522dcc',
            'dev_requirement' => true,
        ),
    ),
);
